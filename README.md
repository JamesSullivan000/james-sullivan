# James Sullivan

James Sullivan has traveled the world and he is an advocate of clean energy and sold &lt;a href=&#34;diesel generators&#34;&gt;https://apelectric.com/generators&lt;/a&gt;. He was previously employed as a site engineer by a leading multinational.